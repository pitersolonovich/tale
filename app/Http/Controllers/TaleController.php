<?php

namespace App\Http\Controllers;

use App\Models\Tale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TaleController extends Controller
{

    public function front()
    {
        return view('tales.load_more');
    }

    public function index()
    {
        $items = Tale::orderBy('created_at','desc')->paginate(5);

        return view('tales.index', compact('items'));
    }


    public function create(Request $request)
    {
        $data = $request->input();
        $item = new Tale($data);
        $item->save();

        if($item){
            return redirect()->route('index');
        }else{
            return back()->withErrors(['msg' => 'saving error']);
        }
    }

    public function edit(Request $request)
    {
      $tale = Tale::find($request['taleId']);
      $tale->name = $request['body'];
      $tale->update();
      return response()->json(['new_body' => $tale->name], 200);
    }

    public function delete($id)
    {
        $tale = Tale::where('id', $id)->first();
        $tale->delete();
        return redirect()->route('index')->with(['message' => 'Successfuly deleted!']);
    }

    function load_data(Request $request)
    {
        if($request->ajax())
        {
            if($request->id > 0)
            {
                $data = DB::table('tales')
                    ->where('id', '<', $request->id)
                    ->orderBy('id', 'DESC')
                    ->limit(2)
                    ->get();
            }
            else
            {
                $data = DB::table('tales')
                    ->orderBy('id', 'DESC')
                    ->limit(2)
                    ->get();
            }

            $output = '';
            $last_id = '';

            if(!$data->isEmpty())
            {
                foreach($data as $row)
                {
                    $output .= '
        <div class="col-5" style="margin: 10px">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">'.$row->name.'</h5>
                    </div>
                </div>
            </div>
        ';
                    $last_id = $row->id;
                }
                $output .= '
        <button type="button" name="load_more_button" class="btn btn-success form-control" data-id="'.$last_id.'" id="load_more_button">Load More</button>
       ';
            }
            else
            {
                $output .= '
        <button type="button" name="load_more_button" class="btn btn-info form-control">No Data Found</button>
       ';
            }
            echo $output;
        }
    }
}
