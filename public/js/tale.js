$(document).ready(function () {

    var taleId = 0;
    var taleBodyElement = null;

    $('.tale').find('.interaction').find('.edit').on('click', function (event) {
        event.preventDefault();
        taleBodyElement = $(this).closest('tr').children('td.two');
        var taleBody = taleBodyElement.text();
        taleId = $(this).closest('tr').children('td.one').text();
        $('#tale-body').val(taleBody);
        $('#edit-modal').modal();
    });

    $('#modal-save').on('click', function () {
        $.ajax({
            method: 'POST',
            url: url,
            data: {body: $('#tale-body').val(), taleId: taleId, _token: token}
        })
            .done(function (msg) {
                $(taleBodyElement).text(msg['new_body']);
                $('#edit-modal').modal('hide');
            });
    });

});