<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('back');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('front', 'TaleController@front')->name('front');
Route::get('index', 'TaleController@index')->middleware('auth')->name('index');
Route::post('create', 'TaleController@create')->middleware('auth')->name('create');
Route::post('edit', 'TaleController@edit')->middleware('auth')->name('edit');
Route::get('delete/{id}', 'TaleController@delete')->middleware('auth')->name('delete');

Route::post('/load_data', 'TaleController@load_data')->name('load_data');